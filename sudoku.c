#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// variables globales parce que flemme

static int RACINE, LONGUEUR, CARRE;
static int *grille;

// utilitaires

static int
get_g(int y, int x, int *g) {
    if (x<0 || y<0 || x>=LONGUEUR || y>= LONGUEUR) return -1;
    return g[y*LONGUEUR+x];
}

int
get(int y, int x) {
    return get_g(y, x, grille);
}

static int
set_g(int y, int x, int n, int *g) {
    if (x<0 || y<0 || x>=LONGUEUR || y>= LONGUEUR) return 0;
    g[y*LONGUEUR+x] = n;
    return 1;
}

int
set(int y, int x, int n) {
    return set_g(y, x, n, grille);
}

static void
get_ligne(int y, int *ligne, int *g) {
    int i;
    for (i=0; i<LONGUEUR; i++) {
        ligne[i] = get_g(y, i, g);
    }
}

static void
get_colonne(int x, int *ligne, int *g) {
    int i;
    for (i=0; i<LONGUEUR; i++) {
        ligne[i] = get_g(i, x, g);
    }
}

static void
set_ligne(int y, int *ligne, int *g) {
    int i;
    for (i=0; i<LONGUEUR; i++) {
         set_g(y, i, ligne[i], g);
    }
}

static void
set_colonne(int x, int *ligne, int *g) {
    int i;
    for (i=0; i<LONGUEUR; i++) {
        set_g(i, x, ligne[i], g);
    }
}

static int
largeur(int x) {
    if(x==0) return 1;
    int largeur = 0;
    while(x) largeur++, x/=10;
    return largeur;
}

void
afficher(void) {
    if(RACINE == 1) {
        printf("+---+\n| %d |\n+---+\n", *grille);
    }
    else {
        int i,j,k,l,m;
        int nombre, espaces;
        int largeur_max = largeur(LONGUEUR);
        for (i=0; i<RACINE; i++) {
            // display RACINE lines
            for (j=0; j<RACINE; j++) {
                //printf(" ");
                for (k=0; k<RACINE; k++) {
                    for (l=0; l<RACINE; l++) {
                        nombre = get(RACINE*i+j, RACINE*k+l);
                        espaces = 1 + largeur_max - largeur(nombre);
                        for(m=0; m<espaces; m++) printf(" ");
                        printf("%d", nombre);
                    }
                    if (k+1 < RACINE) printf(" |");
                }
                printf("\n");
            }
            if(i+1<RACINE) {
                // display a cesure line
                printf("-");
                espaces = largeur_max+1;
                for (j=0; j<RACINE; j++) {
                    for (l=0; l<RACINE; l++) for(m=0; m<espaces; m++) printf("-");
                    if (j+1 < RACINE) printf("+-");
                }
                printf("\n");
            }
        }
    }
    printf("\n");
}

// génération

static void
template(int taille_petit_carre) {
    // cas particuler : sudoku de taille 1
    if (taille_petit_carre == 1) {
        grille = malloc(sizeof(int));
        *grille = 1;
    }
    else {
        int i,j,k,l;
        // malloc la grille
        grille = malloc(CARRE*sizeof(int));
        // remplir la premiere ligne
        for(i=0; i<LONGUEUR; i++) set(0,i,i+1);
        // remplir le premier bloc de lignes à partir de la première ligne
        for(i=1; i<RACINE; i++) {
            for(j=0; j<LONGUEUR; j++) {
                set(i,j, get(i-1, (j+RACINE)%LONGUEUR));
            }
        }
        // remplir les colonnes suivantes
        for(i=0; i<RACINE; i++) {
            for(j=1; j<RACINE; j++) {
                for(k=0; k<RACINE; k++) {
                    for(l=0; l<RACINE; l++) {
                        // mon cerveau a surchauffé
                        set(RACINE*j+k, RACINE*i+l, get(k, RACINE*i+(l+j)%RACINE));
                    }
                }
            }
        }
    }
}

//!\ x >= 0
static int
factorielle(int x) {
    if(x<2) return 1;
    return x * factorielle(x-1);
}

static int*
generer_permutations(int fact) {
    // generer la liste des permutaions possibles
    // afin d'avoir une probabilité égale
    // d'occurence de chaque permutation
    // merci au site rosettacode.org
    // url de la solution (version 2 choisie) : https://rosettacode.org/wiki/Permutations#C
    int i,j, n, pivot;
    // allouer la liste de retour
    int *permutations = malloc(fact*RACINE*sizeof(int));
    // la permutation canonique
    // ex pour RACINE = 3 : 2 1 0
    int *permutation = malloc(RACINE*sizeof(int));
    for(i=0; i<RACINE; i++) {
        permutations[i] = permutation[i] = RACINE-1-i;
    }
    // calculer les permutations
    for(n=0; n<fact; n++) {
        i=1;
        while(permutation[i] > permutation[i-1]) i++;
        j=0;
        while(permutation[j] < permutation[i])j++;
        pivot=permutation[j];
        permutation[j]=permutation[i];
        permutation[i]=pivot;
        i--;
        for (j = 0; j < i; i--, j++) {
            pivot = permutation[i];
            permutation[i] = permutation[j];
            permutation[j] = pivot;
        }
        // stocker la permutation
        for(i=0; i<RACINE; i++) permutations[RACINE*n+i] = permutation[i];
    }
    free(permutation);
    return permutations;
}

static int*
permutation_aleatoire(int *permutations, int fact) {
    return permutations+RACINE*(rand()%fact);
}

static void
permuter_blocs_de_lignes(int *permutations, int fact, int *grille2) {
    // il me semble que c'est inutile de permuter les blocs entre eux
}

static void
permuter_blocs_de_colonnes(int *permutations, int fact, int *grille2) {
    // il me semble que c'est inutile de permuter les blocs entre eux
}

static void
permuter_lignes_du_bloc(int *permutations, int fact, int bloc, int *grille2) {
    int i;
    int *permutation = permutation_aleatoire(permutations, fact);
    int *pivot = malloc(LONGUEUR*sizeof(int));
    // on permute les lignes de grille à partir de grille2
    for(i=0; i<RACINE; i++) {
        get_ligne(RACINE*bloc+permutation[i], pivot, grille2);
        set_ligne(RACINE*bloc+i,              pivot, grille);
    }
    // on met à jour grille2 pour qu'elle soit égale à grille
    for(i=0; i<RACINE; i++) {
        get_ligne(RACINE*bloc+i, pivot, grille);
        set_ligne(RACINE*bloc+i, pivot, grille2);
    }
    free(pivot);
}

static void
permuter_colonnes_du_bloc(int *permutations, int fact, int bloc, int *grille2) {
    int i;
    int *permutation = permutation_aleatoire(permutations, fact);
    int *pivot = malloc(LONGUEUR*sizeof(int));
    // on permute les colonnes de grille à partir de grille2
    for(i=0; i<RACINE; i++) {
        get_colonne(RACINE*bloc+permutation[i], pivot, grille2);
        set_colonne(RACINE*bloc+i,              pivot, grille);
    }
    // on met à jour grille2 pour qu'elle soit égale à grille
    for(i=0; i<RACINE; i++) {
        get_colonne(RACINE*bloc+i, pivot, grille);
        set_colonne(RACINE*bloc+i, pivot, grille2);
    }
    free(pivot);
}

int*
cloner_grille(void) {
    int i, *copie = malloc(CARRE*sizeof(int));
    for(i=0; i<CARRE; i++) copie[i] = grille[i];
    return copie;
}

static void
melanger(void) {
    // random seed
    srand(time(NULL));
    int fact = factorielle(RACINE);
    int *permutations = generer_permutations(fact);
    int i;
    int *copie = cloner_grille();
    permuter_blocs_de_lignes(permutations, fact, copie);
    permuter_blocs_de_colonnes(permutations, fact, copie);
    for(i=0; i<RACINE; i++) {
        permuter_lignes_du_bloc(permutations, fact,  i, copie);
        permuter_colonnes_du_bloc(permutations, fact, i, copie);
    }

    free(permutations);
}

void vider(void);

int
generer(int taille_petit_carre) {
    // on ne peut pas créer un sudoku sans cases
    if (taille_petit_carre < 1) return -1;
    // initialiser les variables globales
    RACINE = taille_petit_carre;
    LONGUEUR = RACINE*RACINE;
    CARRE = LONGUEUR*LONGUEUR;
    // s'il y a déjà un sudoku, on le free
    if (grille) free(grille);
    // remplir le template
    template(taille_petit_carre);
    // mélanger la grille
    melanger();
    // enlever des nombres
    vider();
    return 0;
}

// résolution
void
vider(void) {
// TODO
}

int
main() {
    for(int i=3; i<=3; i++) {
        generer(i);
        afficher();
    }
    return 0;
}
